package de.garantiertnicht.mobexample.entity.firefly;

import javax.annotation.Nonnull;

import de.garantiertnicht.mobexample.MobExample;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.registry.IRenderFactory;

public class Render extends RenderLiving<FireFly> {
    public static final Factory FACTORY = new Factory();

    public Render(RenderManager renderer) {
        super(renderer, new Model(), 0);
    }

    @Override
    @Nonnull
    protected ResourceLocation getEntityTexture(@Nonnull FireFly entity) {
        return new ResourceLocation(MobExample.MODID + ":" + "textures/entity/firefly.png");
    }

    @Override
    protected void preRenderCallback(FireFly entitylivingbaseIn, float partialTickTime) {
        GlStateManager.scale(0.4F, 0.4F, 0.4F);
    }

    public static class Factory implements IRenderFactory<FireFly> {
        @SuppressWarnings("unchecked")
        @Override
        public net.minecraft.client.renderer.entity.Render createRenderFor(RenderManager manager) {
            return new Render(manager);
        }
    }
}
