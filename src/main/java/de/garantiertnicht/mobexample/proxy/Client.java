package de.garantiertnicht.mobexample.proxy;

import de.garantiertnicht.mobexample.entity.firefly.FireFly;
import de.garantiertnicht.mobexample.entity.firefly.Render;
import net.minecraftforge.fml.client.registry.RenderingRegistry;

public class Client implements Proxy {
    @Override
    public void preInit() {
        initRenders();
    }

    private void initRenders() {
        RenderingRegistry.registerEntityRenderingHandler(FireFly.class, Render.FACTORY);
    }
}
