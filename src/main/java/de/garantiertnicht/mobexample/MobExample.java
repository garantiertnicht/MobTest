package de.garantiertnicht.mobexample;

import de.garantiertnicht.mobexample.entity.firefly.FireFly;
import de.garantiertnicht.mobexample.proxy.Proxy;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.init.Biomes;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.EntityRegistry;

@Mod(modid = MobExample.MODID, version = MobExample.VERSION)
public class MobExample {
    public static final String MODID = "mobtest";
    public static final String VERSION = "CI_MOD_VERSION"; // This will incremented automagically, hopefully.

    @SidedProxy(clientSide = "de.garantiertnicht.mobexample.proxy.Client", serverSide = "de.garantiertnicht.mobexample.proxy.Server")
    private static Proxy proxy;

    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        registerEntities();
        proxy.preInit();
    }

    private void registerEntities() {
        byte id = 0;
        EntityRegistry.registerModEntity(FireFly.class, "firefly", id++, this, 64, 1, false, 0x200000, 0xFFFF00);
        EntityRegistry.addSpawn(FireFly.class, 10, 13, 29, EnumCreatureType.AMBIENT, Biomes.FOREST, Biomes.FOREST_HILLS, Biomes.BIRCH_FOREST, Biomes.BIRCH_FOREST_HILLS, Biomes.MUTATED_FOREST, Biomes.MUTATED_BIRCH_FOREST, Biomes.MUTATED_BIRCH_FOREST_HILLS, Biomes.ROOFED_FOREST, Biomes.MUTATED_ROOFED_FOREST);
    }
}
